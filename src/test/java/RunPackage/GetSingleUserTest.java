package RunPackage;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;


public class GetSingleUserTest {
    @Test
    public void single() {
       given()
                .baseUri("https://reqres.in/api/users")
                .basePath("/2")
                .contentType(ContentType.JSON)
               .when().get()
                .then().log().all()
                .statusCode(200)
                .body("data.email", equalTo( "janet.weaver@reqres.in"));
    }
}
