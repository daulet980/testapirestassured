package RunPackage;
import io.restassured.RestAssured;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

public class LoginSocc {
    @Test
    public  void LoginSocc(){
        RestAssured.baseURI = "https://reqres.in/api/login/2";
        String userData = "{" +
                "\"email\" : \"eve.holt@reqres.in\", "+
                "\"password\": \"cityslicka\"" +
                "}";
        given().body(userData)
                .when()
                .post()
                .then().log().all()
                .assertThat()
                .statusCode(201)
                .body("createdAt", is(notNullValue()));
    }
}