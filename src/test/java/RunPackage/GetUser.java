package RunPackage;

import io.restassured.RestAssured;
import org.junit.jupiter.api.Test;


import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;


public class GetUser {
    @Test
    public void getUsers(){
        RestAssured.baseURI = "http://reqres.in/api/users";
        given().
                queryParam("page","2")
                .body("")
                .when()
                .get()
                .then().log().all()
                .assertThat().statusCode(200)
                .body("page", equalTo((2)));
    }
}
