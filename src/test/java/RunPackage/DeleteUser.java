package RunPackage;

import io.restassured.RestAssured;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class DeleteUser {
@Test
    public void    deleteUser() {
    RestAssured.baseURI = "https://reqres.in/api";
    given()
            .when()
            .delete("/users/2")
            .then()
            .statusCode(204);

}
    }

