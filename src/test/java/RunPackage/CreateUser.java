package RunPackage;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class CreateUser {
    @Test
    public  void CreateUser(){
        RestAssured.baseURI = "https://reqres.in/api/users";
        String userData = "{" +
                "\"name\":\"morpheus\"," +
                "\"job\":\"leader\"" +
                "}";
        Response createUser = (Response) given().contentType(io.restassured.http.ContentType.JSON)
                .body(userData).log().all()
                .when()
                .post();
              createUser.then().assertThat().statusCode(201).body("id", is(notNullValue()));
                String createdUserId = createUser.then().extract().path("id");
        System.out.println(createdUserId); // так как апи не совсем реальное, то результат 404

         given()
//                .baseUri()
                 .pathParam("userId", createdUserId)
                .when().log().all()
                .get("https://reqres.in/api/users/{userId}")
                .then().log().all();
                  }
}

